import React from 'react'

class Child extends React.Component{

  render(){
    console.log('props in child component', this.props)
    let { name } = this.props
    return (
   
            <p>Hello my name is {name}</p>
       
   		   )
  }
}
export default Child