import React from 'react';
import Child from './Child'

class App extends React.Component {
  render(){
    return (
      <div>
         <Child name='Andrés'/>
      </div>
    )
  }
}
export default App;